AM_CXXFLAGS := -Wall -std=c++0x -fno-rtti \
	-I$(top_srcdir) -I$(top_srcdir)/src \
	$(LIBCN_CFLAGS) $(OPENMP_CXXFLAGS) \
	-DHAVE_CONFIG_H
