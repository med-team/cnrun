/*
 *       File name:  libstilton/lang.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-03-23
 *
 *         Purpose:  language and gcc macros
 *
 *         License:  GPL
 */

#ifndef CNRUN_LIBSTILTON_LANG_H_
#define CNRUN_LIBSTILTON_LANG_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <cfloat>
#include <cmath>
#include <unistd.h>
#include <stdio.h>

using namespace std;

namespace cnrun {
namespace stilton {

// for functions to suppress some possibly benign exceptions:
enum class TThrowOption {
        do_throw,
        no_throw,
};

typedef unsigned long hash_t;

inline int dbl_cmp( double x, double y) __attribute__ ((pure));
inline int dbl_cmp( double x, double y)  // optional precision maybe?
{
        if ( fabs(x - y) > DBL_EPSILON )
                return (x > y) ? 1 : -1;
        else
                return 0;
}


// g++ bits

#define MAKE_UNIQUE_CHARP(p)				\
        unique_ptr<void,void(*)(void*)> p##_pp(p,free);


#define DELETE_DEFAULT_METHODS(T)		\
        T () = delete;				\
        T (const T&) = delete;			\
        void operator=( const T&) = delete;




// gcc bits

// # define __pure          __attribute__ ((pure))
// # define __const         __attribute__ ((const))
// # define __noreturn      __attribute__ ((noreturn))
// # define __malloc        __attribute__ ((malloc))
// # define __must_check    __attribute__ ((warn_unused_result))
// # define __deprecated    __attribute__ ((deprecated))
// # define __used          __attribute__ ((used))
// # define __unused        __attribute__ ((unused))
// # define __packed        __attribute__ ((packed))
#define likely(x)        __builtin_expect (!!(x), 1)
#define unlikely(x)      __builtin_expect (!!(x), 0)


#define FAFA printf( __FILE__ ":%d (%s): fafa\n", __LINE__, __FUNCTION__);

}
}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
