/*
 *       File name:  libstilton/containers.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-03-23
 *
 *         Purpose:  misc supporting algorithms (containers, Erlangish aliases)
 *
 *         License:  GPL
 */

#ifndef CNRUN_LIBSTILTON_CONTAINERS_H_
#define CNRUN_LIBSTILTON_CONTAINERS_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <list>
#include <forward_list>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

namespace cnrun {
namespace alg {

template <typename T>
bool
member( const T&, const list<T>&) __attribute__ ((pure));

template <typename T>
bool
member( const T& x, const list<T>& v)
{
        return find( v.begin(), v.end(), x) != v.end();
}

template <typename T>
bool
member( const T&, const forward_list<T>&) __attribute__ ((pure));

template <typename T>
bool
member( const T& x, const forward_list<T>& v)
{
        return find( v.begin(), v.end(), x) != v.end();
}

template <typename T>
bool
member( const T&, const vector<T>&) __attribute__ ((pure));

template <typename T>
bool
member( const T& x, const vector<T>& v)
{
        return find( v.begin(), v.end(), x) != v.end();
}

template <typename K, typename V>
bool
member( const K&, const map<K, V>&) __attribute__ ((pure));

template <typename K, typename V>
bool
member( const K& x, const map<K, V>& m)
{
        return m.find(x) != m.end();
}

} // namespace alg
} // namespace cnrun

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
