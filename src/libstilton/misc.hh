/*
 *       File name:  libstilton/misc.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-09-19
 *
 *         Purpose:  misc supporting algorithms
 *
 *         License:  GPL
 */

#ifndef CNRUN_LIBSTILTON_MISC_H_
#define CNRUN_LIBSTILTON_MISC_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <unistd.h>

using namespace std;

namespace cnrun {
namespace stilton {

struct C_verprintf {
        virtual int verbose_threshold() const = 0;
        void vp( int, const char* fmt, ...) const __attribute__ ((format (printf, 3, 4)));
        void vp( int, FILE*, const char* fmt, ...) const __attribute__ ((format (printf, 4, 5)));
};


} // namespace stilton
} // namespace cnrun

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
