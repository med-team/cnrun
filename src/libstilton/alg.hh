/*
 *       File name:  libstilton/alg.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-03-23
 *
 *         Purpose:  misc supporting algorithms
 *
 *         License:  GPL
 */

#ifndef CNRUN_LIBSTILTON_ALG_H_
#define CNRUN_LIBSTILTON_ALG_H_


#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace cnrun {
namespace stilton {
namespace alg {

/// uncomment on demand

// template <typename T>
// inline void
// __attribute__ ((pure))
// pod_swap( T&& a, T&& b)
// {
//         T&& tmp = move(a);
//         a = move(b);
//         b = move(tmp);
// }



template <typename T>
inline bool
__attribute__ ((pure))
overlap( const T& a, const T& b,
         const T& c, const T& d)
{
        return not ((a < c && b < c) || (a > d && b > d));
}

template <typename T>
inline bool
__attribute__ ((pure))
between( const T& a, const T& b, const T&c)
{
        return a <= b && b <= c;
}



template <typename T>
int
__attribute__ ((pure))
sign( T x)
{
        return (x > 0) ? 1 : (x == 0) ? 0 : -1;
}


template <typename T>
void
__attribute__ ((pure))
ensure_within( T& v, const T& l, const T& h)
{
        if ( v < l )
                v = l;
        else if ( v > h )
                v = h;
}

template <typename T>
T
__attribute__ ((pure))
value_within( const T& v, const T& l, const T& h)
{
        T o {v};
        if ( v < l )
                o = l;
        else if ( v > h )
                o = h;
        return o;
}

// for more, check this file in Aghermann

} // namespace alg
} // namespace stilton
} // namespace cnrun

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
