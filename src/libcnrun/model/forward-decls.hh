/*
 *       File name:  libcnrun/model/forward-decls.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-09-16
 *
 *         Purpose:  forward declarations
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_MODEL_FORWARDDECLS_H_
#define CNRUN_LIBCNRUN_MODEL_FORWARDDECLS_H_

namespace cnrun {

class CModel;

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
