/*
 *       File name:  libcnrun/units/forward-decls.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2014-09-16
 *
 *         Purpose:  forward declarations
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_UNITS_FORWARDDECLS_H_
#define CNRUN_LIBCNRUN_UNITS_FORWARDDECLS_H_

namespace cnrun {

class C_BaseUnit;
class C_BaseNeuron;
class C_BaseSynapse;
class C_HostedNeuron;
class C_HostedSynapse;
class C_StandaloneNeuron;
class C_StandaloneSynapse;

class C_HostedConductanceBasedNeuron;
class C_HostedRateBasedNeuron;

class CNeuronMap;
class CSynapseMap;

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
