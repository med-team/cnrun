/*
 *       File name:  libcnrun/units/standalone-synapses.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny <tnowotny@ucsd.edu>
 * Initial version:  2008-08-02
 *
 *         Purpose:  standalone synapses (those not having state vars
 *                   on model's integration vector)
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_UNITS_STANDALONESYNAPSES_H_
#define CNRUN_LIBCNRUN_UNITS_STANDALONESYNAPSES_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <iostream>

#include "base-synapse.hh"
#include "base-neuron.hh"
#include "standalone-attr.hh"
#include "mx-attr.hh"


namespace cnrun {

class CModel;

class C_StandaloneSynapse
  : public C_BaseSynapse, public C_StandaloneAttributes {

        DELETE_DEFAULT_METHODS (C_StandaloneSynapse)

    protected:
        C_StandaloneSynapse (TUnitType, C_BaseNeuron *insource, C_BaseNeuron *intarget,
                             double ing, CModel*, int s_mask = 0);

    public:
        double &var_value( size_t v)                        { return V[v]; }
        const double &get_var_value( size_t v) const        { return V[v]; }
        double  S() const                                   { return V[0]; }
        double &S( vector<double>&)                         { return V[0];  }

        void reset_vars()
                {
                        memcpy( V.data(), __CNUDT[_type].stock_var_values,
                                sizeof(double) * v_no());
                        memcpy( V_next.data(), __CNUDT[_type].stock_var_values,
                                sizeof(double) * v_no());
                }
};





class CSynapseMap
  : public C_StandaloneSynapse {

        DELETE_DEFAULT_METHODS (CSynapseMap)

    public:
        static constexpr double fixed_dt = 0.1;

        CSynapseMap (C_BaseNeuron *insource, C_BaseNeuron *intarget,
                     double ing, CModel*, int s_mask = 0, TUnitType alt_type = YT_MAP);

        void preadvance();  // defined inline in model.h

        enum {
                _tau_, _delta_, _Esyn_
        };
        double Isyn( const C_BaseNeuron &with_neuron, double g) const
                {
                        return -g * S() * (with_neuron.E() - P[_Esyn_]);
                }
        double Isyn( vector<double>& unused, const C_BaseNeuron &with_neuron, double g) const
                {
                        return Isyn( with_neuron, g);
                }

    protected:
        bool _source_was_spiking;
};





class CSynapseMxMap
  : public CSynapseMap, public C_MultiplexingAttributes {

        DELETE_DEFAULT_METHODS (CSynapseMxMap)

    public:
        static constexpr double fixed_dt = 0.1;

        CSynapseMxMap( C_BaseNeuron *insource, C_BaseNeuron *intarget,
                       double ing, CModel *inM, int s_mask = 0)
              : CSynapseMap( insource, intarget, ing, inM, s_mask, YT_MXMAP)
                {}

        enum {
                _tau_, _delta_, _Esyn_
        };
        void preadvance();  // defined inline in model.h

    private:
        friend class CModel;
        void update_queue();
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
