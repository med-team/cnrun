/*
 *       File name:  libcnrun/units/standalone-synapses.cc
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny <tnowotny@ucsd.edu>
 * Initial version:  2009-04-08
 *
 *         Purpose:  standalone synapses.
 *
 *         License:  GPL-2+
 */

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <iostream>

#include "types.hh"
#include "model/model.hh"
#include "standalone-synapses.hh"


cnrun::C_StandaloneSynapse::
C_StandaloneSynapse (TUnitType type_,
                     C_BaseNeuron* source_, C_BaseNeuron* target_,
                     double g_, CModel* M_, int s_mask)
      : C_BaseSynapse (type_, source_, target_, g_, M_, s_mask),
        C_StandaloneAttributes (__CNUDT[type_].vno)
{
        reset_vars();
        if ( M )
                M->include_unit( this);
        // else
        //         _status &= ~CN_UENABLED;
}





const double cnrun::CN_Params_SynapseMap[] = {
//        0.075,
       18.94463,  // Decay rate time constant
        0.25,
        0

};
const char* const cnrun::CN_ParamNames_SynapseMap[] = {
//        "Synaptic strength g, " CN_PU_CONDUCTANCE,
        "Decay rate time constant τ, " CN_PU_RATE,
        "Release quantile δ",
        "Reversal potential Vrev, " CN_PU_POTENTIAL
};
const char* const cnrun::CN_ParamSyms_SynapseMap[] = {
//        "gsyn",
        "tau",
        "delta",
        "Vrev"
};


cnrun::CSynapseMap::
CSynapseMap (C_BaseNeuron *insource, C_BaseNeuron *intarget,
             double ing, CModel *inM, int s_mask, TUnitType alt_type)
      : C_StandaloneSynapse( alt_type, insource, intarget, ing, inM, s_mask),
        _source_was_spiking (false)
{
        if ( !inM )
                throw "A MxMap synapse is created unattached to a model: preadvance() will cause a segfault!";
        if ( isfinite( inM->_discrete_dt) && inM->_discrete_dt != fixed_dt )
                throw "Inappropriate discrete dt\n";
        inM -> _discrete_dt = fixed_dt;
}



void
cnrun::CSynapseMxMap::
update_queue()
{
        size_t k = _source -> n_spikes_in_last_dt();
        while ( k-- )
                _kq.push_back( model_time());

        while ( true ) {
                if ( q() > 0 && model_time() - _kq.front() > P[_tau_] )
                        _kq.erase( _kq.begin());
                else
                        break;
        }
}

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
