/*
 *       File name:  libcnrun/units/hosted-attr.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny <tnowotny@ucsd.edu>
 * Initial version:  2009-03-31
 *
 *         Purpose:  Interface class containing hosted unit attributes.
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_UNITS_HOSTEDATTR_H_
#define CNRUN_LIBCNRUN_UNITS_HOSTEDATTR_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include "libstilton/lang.hh"
#include <vector>


using namespace std;

namespace cnrun {

class C_HostedAttributes {

        friend class CIntegrateRK65;
        friend class CModel;

    protected:
      // variables for units in the model are catenated on a single
      // vector<double>, as an essential optimization measure; each
      // unit knows its own set of variables by this idx:
        size_t idx;
      // the containing model provides idx on registering our unit

    public:
        virtual void reset_vars() = 0;
        virtual double &var_value( size_t) = 0;

        virtual void derivative( vector<double>&, vector<double>&) = 0;
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
