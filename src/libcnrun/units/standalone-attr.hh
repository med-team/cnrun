/*
 *       File name:  libcnrun/units/standalone-attr.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny <tnowotny@ucsd.edu>
 * Initial version:  2009-03-31
 *
 *         Purpose:  Interface class for standalone units.
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_UNITS_STANDALONEATTR_H_
#define CNRUN_LIBCNRUN_UNITS_STANDALONEATTR_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <vector>


using namespace std;
namespace cnrun {

class C_StandaloneAttributes {

    friend class CModel;
    protected:
        C_StandaloneAttributes (size_t nvars)
                {
                        V.resize( nvars);
                        V_next.resize( nvars);
                }

        vector<double>
                V,
                V_next;

    private:
        virtual void preadvance()
                {}
        void fixate()
                {  V = V_next;  }
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
