/*
 *       File name:  libcnrun/units/mx-attr.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny <tnowotny@ucsd.edu>
 * Initial version:  2009-03-31
 *
 *         Purpose:  Interface class for mltiplexing units.
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_UNITS_MXATTR_H_
#define CNRUN_LIBCNRUN_UNITS_MXATTR_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <vector>

using namespace std;

namespace cnrun {

class C_MultiplexingAttributes {

    protected:
        virtual void update_queue() = 0;
        vector<double> _kq;

    public:
        double  q() const
                {
                        return _kq.size();
                }
        void reset()
                {
                        _kq.clear();
                }
};



class C_DotAttributes {
    public:
        virtual double& spikes_fired_in_last_dt() = 0;
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
