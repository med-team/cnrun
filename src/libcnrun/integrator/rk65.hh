/*
 *       File name:  libcnrun/integrator/rk65.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny
 * Initial version:  2008-09-23
 *
 *         Purpose:  A Runge-Kutta 6-5 integrator.
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_INTEGRATOR_RK65_H_
#define CNRUN_LIBCNRUN_INTEGRATOR_RK65_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include <vector>
#include "libstilton/lang.hh"
#include "base.hh"

using namespace std;

namespace cnrun {

class CIntegrateRK65
  : public CIntegrate_base {

        DELETE_DEFAULT_METHODS (CIntegrateRK65)

    public:
        CIntegrateRK65 (double dt_min_ = 1e-6, double dt_max_ = .5, double dt_cap_ = 5,
                        double eps_ = 1e-8,  double eps_abs_ = 1e-12, double eps_rel_ = 1e-6,
                        bool is_owned_ = true)
              : CIntegrate_base (dt_min_, dt_max_, dt_cap_,
                                 eps_, eps_abs_, eps_rel_, is_owned_)
                {}

        void cycle() __attribute__ ((hot));
        void fixate() __attribute__ ((hot));
        void prepare();

    private:
        vector<double> Y[9], F[9], y5;
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
