/*
 *       File name:  libcnrun/integrator/base.hh
 *         Project:  cnrun
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *                   building on original work by Thomas Nowotny
 * Initial version:  2008-09-23
 *
 *         Purpose:  base class for integrators, to be plugged into CModel.
 *
 *         License:  GPL-2+
 */

#ifndef CNRUN_LIBCNRUN_INTEGRATOR_BASE_H_
#define CNRUN_LIBCNRUN_INTEGRATOR_BASE_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#include "libstilton/lang.hh"
#include "../model/forward-decls.hh"


namespace cnrun {

class CIntegrate_base {

        DELETE_DEFAULT_METHODS (CIntegrate_base)

    public:
        double  _dt_min, _dt_max, _dt_cap,
                _eps, _eps_abs, _eps_rel,
                dt;  // that which is current

        bool    is_owned;

        CModel *model;

        CIntegrate_base (const double& dt_min, const double& dt_max, const double& dt_cap,
                         const double& eps, const double& eps_abs, const double& eps_rel,
                         bool inis_owned)
              : _dt_min (dt_min), _dt_max (dt_max), _dt_cap (dt_cap),
                _eps (eps), _eps_abs (eps_abs), _eps_rel (eps_rel),
                dt (dt_min),
                is_owned (inis_owned)
                {}
        virtual ~CIntegrate_base()
                {}

        virtual void cycle() = 0;
        virtual void fixate() = 0;
        virtual void prepare() = 0;
};

}

#endif

// Local Variables:
// Mode: c++
// indent-tabs-mode: nil
// tab-width: 8
// c-basic-offset: 8
// End:
