-- This is an example illustrating how to use cnrun package in Lua.
--
-- 1. After loading the cnrun module with 'require', the first step is
--    to get or create an interpreter context.  It is an opaque light
--    user data object, which you will pass as the first argument to
--    all subsequent calls to cnrun functions.
--
-- 2. You can create and keep multiple models in a context, modify and
--    advance them independently. Models are identified by a label (a
--    string).
--
-- 3. On error, all cnrun methods return two values: first a nil,
--    second a string describing what went wrong.  On success, the
--    first value is 1 (an integer), and the rest are method-specific.
--
-- 4. Don't lose the context object.  It will not be gabage-collected
--    for you (it is a C++ thing).

local effective_unpack = table.unpack
if _VERSION == "Lua 5.1" then
   effective_unpack = unpack
end

local M = require("cnrun")

local res, ult, result
local C, model

function Pw (str) print ("\027[01;1m" .. str  .. "\027[00m") end
function Pg (str) print ("\027[01;32m" .. str  .. "\027[00m") end
function Pr (str) print ("\027[01;31m" .. str  .. "\027[00m") end
function Pok ()
   print ("\027[01;34m" .. "ok" .. "\027[00m")
   print ()
end

function ASSERT (a1, a2)
   res, ult = a1, a2
   if res == nil then
      Pr (ult)
      os.exit (2)
   end
   Pok ()
end

function ASSERT_FALSE (a1, a2)
   res, ult = a1, a2
   if res ~= nil then
      Pr (res)
      Pr (ult)
      os.exit (2)
   end
   Pg ("ok (" .. ult .. ")")
   print ()
end


function print_units (model_name, ult, unit_regex)
   Pg ("There are " .. #ult .. " unit(s) matching \"" .. unit_regex .. "\"")
   local unit_list = ult
   local fmt = " %-10s %-16s %-16s %-12s %-16s %-6s"
   Pg (string.format(
          fmt,
          "label", "class", "family", "species", "has_sources", "is_altered"))
   Pg (string.rep('-', 87))
   for _, u in ipairs(unit_list) do
      result = {M.get_unit_properties (C, model_name, u)}
      res, ult = result[1], {effective_unpack(result, 2)}
      local b = function (x) if x then return "yes" else return "no" end end
      Pg (string.format(
             fmt,
             ult[1], ult[2], ult[3], ult[4], b(ult[5]), b(ult[6])))
   end
   print()
end


function compare_models (model_name1, model_name2)
   local result1 = {M.get_units_matching(C, model_name1, ".*")}
   local result2 = {M.get_units_matching(C, model_name2, ".*")}
   res, ult1 = result[1], {effective_unpack(result1, 2)}
   ASSERT (res, ult1)
   res, ult2 = result[1], {effective_unpack(result2, 2)}
   ASSERT (res, ult2)

   local relevant_list1 = get_relevant_unit_params (model_name1, ult1)
   local relevant_list2 = get_relevant_unit_params (model_name2, ult2)

   return relevant_list1 == relevant_list2
end


function get_relevant_unit_params (model_name, unit_list)
   local rup = {}
   for _, u in ipairs(unit_list) do
      result = {M.get_unit_properties (C, model_name, u)}
      res, ult = result[1], {effective_unpack(result, 2)}
      table.insert (rup, {ult[1], ult[2], ult[3], ult[4], ult[5], ult[6]})
   end
   return rup
end


-- main

local mname = "FAFA"
local verbosely = 3

do
   Pw ("Dump units")
   M.dump_available_units ()
end

Pw ("Obtain context")
ASSERT (M.get_context ())
C = ult

Pw ("Create model")
ASSERT (M.new_model (C, mname))
model = ult

M.set_model_parameter (C, mname, "verbosely", verbosely)

result = {M.list_models (C)}
ASSERT (result[1], {effective_unpack(result, 2)})

do
   Pg ("Model(s):")
   local model_list = ult
   Pg (table.concat(model_list))
   print ()
end

Pw ("Import NML")
ASSERT (M.import_nml (C, mname, "m.nml"))


do
   Pw ("Testing NML export")
   local mname2 = "FAFA2"
   -- cwd is specially created for us in test target: ./ is entirely
   -- at our disposal
   local f2 = "./m2.nml"

   Pw ("Exporting to " .. f2)
   ASSERT (
      M.export_nml (C, mname, f2))

   M.new_model (C, mname2)
   Pw ("Importing into new model from " .. f2)
   ASSERT (
      M.import_nml (C, mname2, f2))

   Pw ("Comparing old and new models")
   if compare_models (mname, mname2) then
      print ()
      Pg ("Models appear to be different:")
      Pg ("Original model:")
      print_units (mname, ult1, ".*")
      Pg ("Reimported model:")
      print_units (mname2, ult2, ".*")
      return 1
   end
   Pg ("Models are pretty much the same")
   print ()

   ASSERT (M.delete_model (C, mname2))
end


do
   Pg ("Host parmeters:")
   local parameters = {
      "verbosely", "integration_dt_min",
      "integration_dt_max", "integration_dt_cap",
      "listen_dt", "listen_mode",
      "sxf_start_delay", "sxf_period", "sdf_sigma"
   }
   local fmt = " %22s: %-q"
   for i,p in ipairs(parameters) do
      res, ult = M.get_model_parameter (C, mname, p)
      Pg (string.format (fmt, p, ult))
   end
   print ()
end


do
   Pw ("Delete nonexistent model")
   ASSERT_FALSE (M.delete_model (C, "fafa moo"))
end


do
   local regexp = "L.*"
   Pw ("Get units matching \"" .. regexp .. "\"")
   result = {M.get_units_matching(C, mname, regexp)}
   ASSERT (
      result[1], {effective_unpack(result, 2)})
   print_units (mname, ult, "L.*")
end


do
   Pw ("Advance 10 sec")
   ASSERT (M.advance (C, mname, 10000))
end


do
   Pw ("Get parameter")
   local u, p, v0, v9, vr = "LNz.0", "gNa"
   ASSERT (M.get_unit_parameter (C, mname, u, p))
   v0 = ult

   Pw ("Set new value")
   ASSERT (M.set_unit_parameter (C, mname, u, p, v0 * 2))

   Pw ("Get again to check")
   ASSERT (M.get_unit_parameter (C, mname, u, p))
   v9 = ult

   Pw ("Revert to stock")
   ASSERT (M.revert_matching_unit_parameters (C, mname, u))
   local count_reset = ult
   ASSERT (M.get_unit_parameter (C, mname, u, p))
   vr = ult
   Pg (string.format(
             ".. changed %s of %s from %g to %g, then reset (%d affected) to %g\n",
             p, u, v0, v9, count_reset, vr))
end


do
   Pw ("Modify parameter in bulk:")
   local us, ut, gsyn = "LNz.0", "LN1.0"
   ASSERT (M.set_matching_synapse_parameter (C, mname, us, ut, "gsyn", 4.2))
   Pg (string.format(
             ".. changed gsyn of synapse connecting %s to %s, to %g\n",
             us, ut, 4.2))
end


do
   Pw ("Describe")
   ASSERT (M.describe_model (C, mname))
end


do
   Pw ("Advance, check state variables")
   for i = 1, 6, 1 do
      M.advance (C, mname, 1000)
      result = {M.get_unit_vars (C, mname, "LNz.0")}
      res, ult = result[1], {effective_unpack(result, 2)}
      Pg (table.concat(ult, '; '))
   end
   print()
end


do
   Pw ("Putout")
   local affected, remaining
   result = {M.get_units_matching(C, mname, ".*")}
   res, ult = result[1], {effective_unpack(result, 2)}
   local unit_list = ult
   math.randomseed(os.time())
   local deleting = unit_list[math.random(1, #unit_list)]
   -- deleting, _ = string.gsub(deleting, ".", "\\.")
   ASSERT (M.putout (C, mname, deleting))
   Pg (string.format(".. deleted unit %s", deleting))
   print()
end


do
   Pw ("Decimate")
   local affected, remaining
   ASSERT (M.decimate (C, mname, "L.*", 0.3))
   affected, remaining = ult
   remaining = #{M.get_units_matching (C, mname, ".*")} - 1
   Pg (string.format(
          ".. %d units gone, %d remaining",
          affected, remaining))
   print()
end


do
   Pw ("Delete model")
   ASSERT (M.delete_model (C, mname))
   Pg ("Model ".. ult .. " deleted")

   Pw ("Drop context")
   ASSERT (M.drop_context (C))
end

print ("Test passed")
